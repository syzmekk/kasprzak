## DevOps:

![Facebook](https://childsvoice.org/wp-content/uploads/2016/01/facebook-logo-200x200.png)
![Uber](https://media.licdn.com/dms/image/C4D0BAQFiYnR1Mbtxdg/company-logo_200_200/0?e=2159024400&v=beta&t=s1FwUxsf5Q4A0YaCWdwScM8FiV0yGGvASHrTpZmxrMU)
![Apple](https://www.freepngimg.com/thumb/apple_logo/25366-7-apple-logo-file-thumb.png)
<img src="https://cdn.dobreprogramy.pro/wp-content/cache/download-manager/Netflix-c510b0335a9745b2a1ab1d33631e3272-256x0.png" width="200" alt="Netflix">
<img src="https://s4840.pcdn.co/wp-content/uploads/2017/12/Airbnb-ballarat.png" width="200" alt="Airbnb">

Note:

- **Facebook i Instagram** - UX/UI design, frontend, wielka ilość danych, down około 7 razy od powstania (2004)!
- **Uber** - 400% z 2013 na 14 i 100% z 14 na 15
- **Apple** - tworzenie oprogramowania dla userów, łatki i patche
- **Airbnb** - analityka danych, big data
