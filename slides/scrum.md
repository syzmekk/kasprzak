Scrum

![scrum](https://dzone.com/storage/temp/4290023-image1.png)

Note:

- Iteracja
- Sprinty
- PO, SM i Dev Team
- planning, daily, refinement, review i retro
- przejrzystość
- inspekcja zespołu, produktu, organizacji
- adaptacja planów i dostosowanie procesu
- długość od 1 do 4 tygodni (2 najlepiej)

>Najskuteczniejszą metodą, żeby Scrum nie zadziałał jest unikanie przejrzystości (brak transparencji), informacji zwrotnej (brak inspekcji) i podejmowania decyzji na podstawie tych informacji (brak adaptacji).
