#### Whoami

`Szymon Maciejewski`

![Kasprzak](http://www.kasprzak.edu.pl/logo_k.png)
![PJATK](http://www.numerfaksu.pl/wp-content/uploads/2015/08/logo-pjatk.png)
![Accenture](https://media.licdn.com/dms/image/C4D0BAQGbGXCvku4m_w/company-logo_200_200/0?e=2159024400&v=beta&t=naEDur5oR-Y8xEC0Vsj8I_Jq5qJKihxa23EJGqAWe9w)

- Absolwent Zespołu Szkół nr 36 im. Marcina Kasprzaka
- Student Polsko-Japońskiej Akademii Technik Komputerowych
- Analityk DevOps w Accenture (Technology)
