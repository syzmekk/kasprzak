### Agile

Note:

- Powstało w 2001 roku

- 12 zasad zwinnego programowania:
  - Najwyższy priorytet ma dla nas zadowolenie klienta
    dzięki wczesnemu i ciągłemu wdrażaniu
    wartościowego oprogramowania.

  - Bądźcie gotowi na zmiany wymagań
    nawet na późnym etapie jego rozwoju.
    Procesy zwinne wykorzystują zmiany
    dla zapewnienia klientowi konkurencyjności.

  - Dostarczajcie funkcjonujące oprogramowanie często,
    w kilkutygodniowych lub kilkumiesięcznych odstępach.
    Im częściej, tym lepiej.

  - Zespoły biznesowe i deweloperskie muszą ściśle ze sobą
    współpracować w codziennej pracy przez cały czas trwania
    projektu.

  - Twórzcie projekty wokół zmotywowanych ludzi.
    Zapewnijcie im potrzebne środowisko oraz wsparcie i zaufajcie, że
    wykonają powierzone zadanie.

  - Najbardziej efektywnym i wydajnym sposobem przekazywania
    informacji zespołowi deweloperskiemu i wewnątrz niego jest
    rozmowa twarzą w twarz.

  - Działające oprogramowanie jest podstawową miarą postępu.

  - Procesy zwinne umożliwiają zrównoważony rozwój.
    Sponsorzy, deweloperzy oraz użytkownicy powinni być w stanie
    utrzymywać równe tempo pracy.

  - Ciągłe skupienie na technicznej doskonałości i dobrym
    projektowaniu zwiększa zwinność.

  - Prostota – sztuka minimalizowania ilości koniecznej pracy – jest kluczowa.

  - Najlepsze rozwiązania architektoniczne, wymagania i projekty
    pochodzą od samoorganizujących się zespołów.

  - W regularnych odstępach czasu zespół analizuje możliwości
    poprawy swojej wydajności, a następnie dostraja i dostosowuje
    swoje działania do wyciągniętych wniosków.
