Kanban

![kanban](https://d30s2hykpf82zu.cloudfront.net/wp-content/uploads/2018/11/Kanban-Development-Board.png)

Note:

- **Lean** - maksymalizowanie wartości dla klienta przy minimalnym nakładzie pracy i wykorzystaniu materiałów; innymi słowy – minimalizowaniu strat
- Definition of Done
- Work in progress
- backlog
- limity górne i dolne
- przejrzystość i otwartość
- współpraca
- PO i SM
