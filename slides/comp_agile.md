## Agile:

![Amazon](http://www.johnstonefitness.com/wp-content/uploads/2016/04/amazon-icon.png)
![Netflix](https://cdn.dobreprogramy.pro/wp-content/cache/download-manager/Netflix-c510b0335a9745b2a1ab1d33631e3272-256x0.png)
![Spotify](https://dashboard.snapcraft.io/site_media/appmedia/2017/12/spotify-linux-256.png)

Note:

- **Amazon** - 40% nieużywanych serwerów w ciągu roku czekających na black friday czy też święta, provider clouda
- **Netflix** - tysiące wdrożeń dziennie, mikroserwisy z autoskalowaniem, 125M userów, 140M+ godz. oglądanych/dzień
- **Spotify** - live streaming on demand, około 2010 nagły wzrost, 40 inżynierów, 4 opsów i tuziny systemów, ogromny technical debt, brak odpowiednich narzędzi
- **Spotify w liczbach** - 55 rynków, 20M+ piosenek, 1,5MLD playlist, 24M userów, 6M+ płacących
