# Nie po to studiowałem informatykę, żeby teraz pracować z ludźmi

> Szymon Maciejewski - Warsztaty z dnia 07.10.2019r.

## Instalacja

### Pełny setup

1. Zainstaluj [Node.js](http://nodejs.org/) (4.0.0 lub nowszy)

2. Sklonuj repozytorium
   - Używając HTTPS

		```sh
		$ git clone https://gitlab.com/syzmekk/kasprzak.git
		```

	- Używając SSH

		```sh
		$ git clone git@gitlab.com:syzmekk/kasprzak.git
		```

3. Wejdź do folderu z prezentacją

   ```sh
   $ cd kasprzak
   ```

4. Zainstaluj zależności

   ```sh
   $ npm install
   ```

5. Otwórz prezentację i monitoruj pliki źródłowe przy zmianach

   ```sh
   $ npm start
   ```

6. Otwórz <http://localhost:8000> aby zobaczyć prezentację

   Możesz zmienić port używając `npm start -- --port=8001`.

### Disclaimer

Prezentacja została stworzona przy użyciu [reveal.js](https://github.com/hakimel/reveal.js).
